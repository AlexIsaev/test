﻿<?php 
//Класс прямоугольника
class Rectangle 
{
    //Объявление переменных для прямоугольника
    public $rw = 0;
    public $rh = 0;
    
    //Функция для подсчета площади прямоугольника + вывод данных
    public function count($width,$height)
	{
        echo nl2br("Ширина прямогуольника = $width\r\n Высота прямогуольника = $height\r\n");
        $square = $width * $height;
        echo nl2br("Площадь прямоугольника: ".$square."\r\n\r\n");
        return $square;
    }
}

//Класс круг
class Circle
{
    //Объявление переменных для треугольника + вывод данных
    public $cr = 0; //Радиус круга
        
    //Функция для подсчета площади круга 
    public function count($cr)
	{
        $p = 3.14; //Число Пи
        echo nl2br("Радиус круга = $cr\r\n");
        $square = $cr*$p;
        echo nl2br("Площадь круга: ".$square."\r\n\r\n");
        return $square;
    }
}

//Класс пирамида
class Pyramid
{
    //Объявление переменных для пирамиды
    public $pb = 0; //Сторона основания пирамиды pyramidBase
    public $ph = 0; //Высота боковых треугольников пирамиды pyramidHeight
    //Функция для подсчета площади пирамиды + вывод данных
    public function count($ph,$pb)
	{
        $st = $pb*$ph/2; //Площадь одного бокового треугольника
        echo nl2br("Сторона = $pb\r\n");
        echo nl2br("Высота боковых треугольников = $ph\r\n");
		echo nl2br("Площадь бокового треугольника = $st\r\n");
        $square = $st*4+$pb*$pb;
        echo nl2br("Площадь пирамиды: ".$square."\r\n\r\n");
        return $square;
    }
}

//Имя файла
$file = 'figures.txt';
if (file_exists($file)){
    file_put_contents($file, "");
} else {
    $op = fopen($file, 'w');
	fclose($op);
}
$nl="\r\n";
//------------------------------------------------------------------------------------------------
//Создание объекта "Прямоугольник" со случайными координатами
$rec = new Rectangle;
$rec->rw = (random_int(1, 10));
$rec->rh = (random_int(1, 10));

//Создание объекта "Круг" со случайными координатами
$cir = new Circle;
$cir->cr = (random_int(1, 10));

//Создание объекта "Пирамида" со случайными координатами
$pyr = new Pyramid;
$pyr->ph = (random_int(1, 10));
$pyr->pb = (random_int(1, 10));

//Запись объекта прямоугольник в файл с помощью serialize
$reccors = serialize($rec)."\r\n"; 
$fp = fopen($file, "a"); 
fwrite($fp, $reccors); 
fclose($fp);

//Запись объекта круг в файл с помощью serialize
$circors = serialize($cir)."\r\n";
$fp = fopen($file, "a"); 
fwrite($fp, $circors); 
fclose($fp);

//Запись объекта пирамида в файл с помощью serialize
$pyrcors = serialize($pyr);
$fp = fopen($file, "a"); 
fwrite($fp, $pyrcors); 
fclose($fp);

//Считывание объектов из файла
$lines = file($file);
$rec = unserialize($lines[0]);
$cir = unserialize($lines[1]);
$pyr = unserialize($lines[2]);

//Сравнение площадей
$s1 = $rec->count($rec->rw, $rec->rh);
$s2 = $cir->count($cir->cr);
$s3 = $pyr->count($pyr->ph, $pyr->pb);
echo nl2br("Площади в порядке убывания:\r\n");

if ($s1 > $s2 and $s1 > $s3) {
    $max = $s1;
} elseif ($s2 > $s3 ) {
    $max = $s2;
} else {
    $max = $s3;
}

//Поиск среднего и наименьшего значения
if ($max == $s3) {
    if ($s1 > $s2) {
        $mid = $s1;
        $low = $s2;
    } else {
        $mid = $s2;
        $low = $s1;
    } 
} elseif ($max == $s2) {
    if ($s1 > $s3) {
        $mid = $s1;
        $low = $s3;
    } else {
        $mid = $s3;
        $low = $s1;
    } 
} elseif ($max == $s1) {
    if ($s2 > $s3) {
        $mid = $s2;
        $low = $s3;
    } else {
        $mid = $s3;
        $low = $s2;
    }
}

echo nl2br("$max\r\n");
echo nl2br("$mid\r\n");
echo nl2br("$low\r\n");

//(random_int(1, 10)) можно заменить на rand() для чуть более случайных результатов

?>

